const MockShopConfig = {
  APP: {
    PORT: 3000,
    ROUTE: `http://localhost:3000`,
  },
  SERVER: {
    PORT: 8080,
    ROUTE: `http://localhost:8080`,
  },
};

export default MockShopConfig;
