import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { MockShopApp } from "./mock-shop-app/mock-shop-app";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <MockShopApp />
    </BrowserRouter>
  </React.StrictMode>
);
