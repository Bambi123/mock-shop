import MockShopConfig from "mock-shop-config";

const methodTypes = ["GET", "POST", "DELETE", "UPDATE", "PUT", "PATCH"];

const nullFunction = () => {};

const configureRoute = (domain, route) => {
  return `${domain}${route}`;
};

const configureMethod = (method) => {
  return methodTypes.includes(method) ? method : "GET";
};

const configureFetchBody = (method, data) => {
  return method === "PUT" || method === "POST"
    ? {
        method: method,
        headers: {
          "Content-Type": "application/json",
        },
        body: data,
      }
    : {
        method: method,
        headers: {
          "Content-Type": "application/json",
        },
      };
};

export const callMockShopServer = (
  method,
  route,
  data = JSON.stringify({}),
  setResponse = nullFunction,
  setError = nullFunction,
  callback = nullFunction
) => {
  const routeCall = configureRoute(MockShopConfig.SERVER.ROUTE, route);
  const methodCall = configureMethod(method);
  const fetchCall = configureFetchBody(methodCall, data);

  fetch(routeCall, fetchCall)
    .then((res) => {
      return res.json();
    })
    .then((res) => {
      setResponse(res);
      callback();
    })
    .catch((error) => {
      setResponse({ code: 99, payload: null });
      setError(error);
    });
};
