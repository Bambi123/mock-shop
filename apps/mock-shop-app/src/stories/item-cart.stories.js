import React from "react";

import { ItemCart } from "../components/item-cart/item-cart";

export default {
  title: "Mock Shop/ItemCart",
  component: ItemCart,
};

export const UsersItemCart = () => <ItemCart n={10} />;
