import React from "react";

import { Button } from "../components/button/button";

export default {
  title: "Mock Shop/Button",
  component: Button,
};

export const DarkButton = () => (
  <Button
    theme={"dark"}
    text={"This is a Primary Button"}
    size={"small"}
    callback={() => {}}
  />
);

export const LightButton = () => (
  <Button
    theme={"light"}
    text={"This is a Secondary Button"}
    size={"small"}
    callback={() => {}}
  />
);

export const HeaderLinkButtonExample = () => (
  <Button
    theme={"dark"}
    text={"mock merch"}
    size={"small"}
    callback={() => {}}
  />
);

export const HeaderLinkProfileExample = () => (
  <Button
    theme={"light"}
    text={"already have an account? sign in!"}
    size={"small"}
    callback={() => {}}
  />
);
