import React from "react";

import { MockShopLogo } from "../components/mock-shop-logo/mock-shop-logo";

export default {
  title: "Mock Shop/Logo",
  component: MockShopLogo,
};

export const PrimaryDark = () => <MockShopLogo type="primary" theme="dark" />;
export const PrimaryLight = () => <MockShopLogo type="primary" theme="light" />;
export const SecondaryDark = () => (
  <MockShopLogo type="secondary" theme="dark" />
);
export const SecondaryLight = () => (
  <MockShopLogo type="secondary" theme="light" />
);

export const LandingDark = () => <MockShopLogo type="landing" theme="dark" />;
export const LandingLight = () => <MockShopLogo type="landing" theme="light" />;
