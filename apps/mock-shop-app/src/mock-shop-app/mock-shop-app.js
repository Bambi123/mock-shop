import React from "react";
import { MockShopFramework } from "../mock-shop-framework/mock-shop-framework";
import { MockShopContext, MockShopContextValue } from "./mock-shop-app-context";
import { Routes, Route, Navigate, Outlet } from "react-router-dom";
import { MockShopLandingPage } from "./mock-shop-app-routes/mock-shop-landing";
import { MockShopBeersPage } from "./mock-shop-app-routes/mock-shop-beers";
import { MockShopMerchPage } from "./mock-shop-app-routes/mock-shop-merch";
import { MockShopAboutPage } from "./mock-shop-app-routes/mock-shop-about";
import { MockShopLogInPage } from "./mock-shop-app-routes/mock-shop-log-in";
import { MockShopCreateAccountPage } from "./mock-shop-app-routes/mock-shop-create-account";
import { MockShopCartPage } from "./mock-shop-app-routes/mock-shop-cart";
import { MockShopSuccessPage } from "./mock-shop-app-routes/mock-shop-success";
import "./mock-shop-app.css";

const MockShopGuestRoutes = () => {
  return true ? <Outlet /> : <Navigate to="/" />;
};

const MockShopUserRoutes = () => {
  const signedIn = true;

  return signedIn ? <Outlet /> : <Navigate to="/log%20in" />;
};

const MockShopGeneralRoutes = () => {
  return <Outlet />;
};

const MockShopAppRoutes = () => {
  return (
    <Routes>
      <Route element={<MockShopGuestRoutes />}>
        <Route path="/log%20in" element={<MockShopLogInPage />} />
        <Route
          path="/create%20account"
          element={<MockShopCreateAccountPage />}
        />
      </Route>
      <Route element={<MockShopUserRoutes />}>
        <Route path="/my%20cart" element={<MockShopCartPage />} />
        <Route path="/success" element={<MockShopSuccessPage />} />
      </Route>
      <Route element={<MockShopGeneralRoutes />}>
        <Route path="/" element={<MockShopLandingPage />} />
        <Route path="/about%20us" element={<MockShopAboutPage />} />
        <Route path="/mock%20beers" element={<MockShopBeersPage />} />
        <Route path="/mock%20merch" element={<MockShopMerchPage />} />
      </Route>
    </Routes>
  );
};

export const MockShopApp = () => {
  return (
    <div className="mock-shop-app">
      <MockShopContext.Provider value={MockShopContextValue}>
        <MockShopFramework />
        <MockShopAppRoutes />
      </MockShopContext.Provider>
    </div>
  );
};
