import { createContext } from "react";

export const MockShopContext = createContext(null);

export const MockShopContextValue = {
  msg: "Hello World!",
};
