import { Grid, Typography } from "@mui/material";
import React, { useState, useEffect } from "react";
import { MuiMatrixSpacer } from "../../helpers/mui-grid-spacers/mui-matrix-spacer";
import { callMockShopServer } from "../../mock-shop-api/mock-shop-api";

export const MockShopAboutPage = () => {
  const [error, setError] = useState(null);
  const [response, setResponse] = useState({
    payload: "<have not received response>",
  });

  const callback = () => {
    console.log("put was successful!");
  };

  useEffect(() => {
    callMockShopServer(
      "GET",
      "/about%20us",
      {},
      setResponse,
      setError,
      () => {}
    );
  }, []);

  useEffect(() => {
    callMockShopServer(
      "PUT",
      "/mailing%20list",
      JSON.stringify({
        Email: "thomas.french0@outlook.com",
      }),
      () => {},
      setError,
      callback
    );
  }, []);

  return (
    <Grid container rowSpacing={1}>
      <MuiMatrixSpacer n={17} />
      <Grid item xs={12}>
        <Typography sx={{ color: "#000000" }}>
          {`THIS IS THE ABOUT PAGE.  Response is: ${response.payload} and error: ${error}`}
        </Typography>
      </Grid>
    </Grid>
  );
};
