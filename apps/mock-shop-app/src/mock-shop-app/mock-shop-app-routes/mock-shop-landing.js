import React from "react";
import { useNavigate } from "react-router-dom";
import { Grid } from "@mui/material";
import { Button } from "../../components/button/button";
import { MockShopLogo } from "../../components/mock-shop-logo/mock-shop-logo";
import { MuiColumnSpacer } from "../../helpers/mui-grid-spacers/mui-column-spacer";
import { MuiMatrixSpacer } from "../../helpers/mui-grid-spacers/mui-matrix-spacer";

const MockShopLandingPageButton = ({ text, callback }) => {
  return <Button theme="dark" text={text} size={"large"} callback={callback} />;
};

export const MockShopLandingPage = () => {
  const navigate = useNavigate();

  return (
    <Grid container rowSpacing={1}>
      <MuiMatrixSpacer n={20} />
      <Grid item xs={12}>
        <MockShopLogo type="landing" theme="dark" />
      </Grid>

      <MuiMatrixSpacer n={5} />

      <Grid container item xs={12}>
        <Grid item xs={5} display={"flex"} justifyContent={"right"}>
          <MockShopLandingPageButton
            text={"shop mock beer"}
            callback={() => {
              navigate("/mock%20beers");
            }}
          />
        </Grid>

        <MuiColumnSpacer xs={2} />

        <Grid item xs={5} display={"flex"} justifyContent={"left"}>
          <MockShopLandingPageButton
            text={"shop mock merch"}
            callback={() => {
              navigate("/mock%20merch");
            }}
          />
        </Grid>
      </Grid>
    </Grid>
  );
};
