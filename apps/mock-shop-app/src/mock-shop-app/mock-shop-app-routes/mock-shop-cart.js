import { Grid, Typography } from "@mui/material";
import React from "react";
import { MuiMatrixSpacer } from "../../helpers/mui-grid-spacers/mui-matrix-spacer";

export const MockShopCartPage = () => {
  return (
    <Grid container rowSpacing={1}>
      <MuiMatrixSpacer n={7} />
      <Grid item xs={12}>
        <Typography sx={{ color: "#000000" }}>THIS IS THE CART PAGE</Typography>
      </Grid>
    </Grid>
  );
};
