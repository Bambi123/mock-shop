import React from "react";
import { Button as MuiButton } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material";
import "../../styles/fonts.css";

const sizeTypes = ["small", "medium", "large"];
const themeTypes = ["dark", "light"];

const DarkTheme = {
  backgroundColor: "#414141",
  color: "#FFFFFF",
};

const LightTheme = {
  backgroundColor: "#FFFFFF",
  color: "#414141",
};

const HoverTheme = {
  backgroundColor: "#E1E1E1",
  color: "#414141",
};

export const Button = ({ theme, text, size, callback }) => {
  const buttonMuiTheme = createTheme({
    typography: {
      fontFamily: "Quantico",
    },
  });

  const sizeType = sizeTypes.includes(size) ? size : "null";

  const useTheme = themeTypes.includes(theme) ? theme : "dark";

  return (
    <ThemeProvider theme={buttonMuiTheme}>
      <MuiButton
        sx={{
          fontSize: "20px",
          minHeight: "30px",
          boxShadow: 0,
          textTransform: "lowercase",
          borderRadius: 3,
          backgroundColor:
            useTheme === "light"
              ? LightTheme.backgroundColor
              : DarkTheme.backgroundColor,
          color: useTheme === "light" ? LightTheme.color : DarkTheme.color,
          "&:hover": {
            backgroundColor:
              useTheme === "light"
                ? HoverTheme.backgroundColor
                : HoverTheme.backgroundColor,
            color: useTheme === "light" ? HoverTheme.color : HoverTheme.color,
          },
        }}
        size={sizeType}
        variant="contained"
        onClick={() => {
          callback();
        }}
      >
        {text}
      </MuiButton>
    </ThemeProvider>
  );
};

Button.defaultProps = {
  theme: "dark",
  text: "",
  size: "null",
  callback: () => {},
};
