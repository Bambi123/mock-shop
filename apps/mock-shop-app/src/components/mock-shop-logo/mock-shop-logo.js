import React from "react";
import "./mock-shop-logo.css";

const themeTypes = ["light", "dark"];
const typeTypes = ["primary", "secondary", "landing"];

export const MockShopLogo = ({ type, theme, callback }) => {
  const useTheme = themeTypes.includes(theme) ? theme : "light";
  const useType = typeTypes.includes(type) ? type : "primary";

  return (
    <div
      className={`logo logo-size-${useType} logo-${useTheme}`}
      onClick={() => {
        callback();
      }}
    >
      Mock Shop
    </div>
  );
};

MockShopLogo.defaultProps = {
  type: "primary",
  theme: "light",
  callback: () => {},
};
