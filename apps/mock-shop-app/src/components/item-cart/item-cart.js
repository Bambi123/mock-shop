import React from "react";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { Badge } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material";

export const ItemCart = ({ n }) => {
  const cartMuiTheme = createTheme({
    typography: {
      fontFamily: "Quantico",
    },
  });

  return (
    <ThemeProvider theme={cartMuiTheme}>
      <Badge
        badgeContent={n}
        color="primary"
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <ShoppingCartIcon sx={{ color: "#FFFFFF" }} />
      </Badge>
    </ThemeProvider>
  );
};

ItemCart.defaultProps = {
  n: 0,
};
