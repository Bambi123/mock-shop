import React from "react";
import { MuiRowSpacer } from "./mui-row-spacer";

export const MuiMatrixSpacer = ({ n }) => {
  return (
    <React.Fragment>
      {[...Array(n)].map(() => (
        <MuiRowSpacer />
      ))}
    </React.Fragment>
  );
};
