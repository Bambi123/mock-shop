import React from "react";
import { Grid } from "@mui/material";

export const MuiColumnSpacer = ({ xs }) => {
  return <Grid item xs={xs} />;
};
