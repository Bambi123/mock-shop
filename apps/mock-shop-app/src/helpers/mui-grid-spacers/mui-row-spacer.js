import React from "react";
import { Grid } from "@mui/material";

export const MuiRowSpacer = () => {
  return <Grid item xs={12} />;
};
