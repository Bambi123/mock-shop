import React from "react";
import { AppBar, Toolbar, Grid } from "@mui/material";
import { MockShopLogo } from "../components/mock-shop-logo/mock-shop-logo";
import { ItemCart } from "../components/item-cart/item-cart";
import { Button } from "../components/button/button";
import { useNavigate } from "react-router-dom";

const MockShopFrameworkLogo = () => {
  const navigate = useNavigate();
  <Grid item xs={2}>
    <MockShopLogo
      type="primary"
      theme="light"
      callback={() => {
        navigate("/");
      }}
    />
  </Grid>;
  return (
    <Grid item xs={2}>
      <MockShopLogo
        type="primary"
        theme="light"
        callback={() => {
          navigate("/");
        }}
      />
    </Grid>
  );
};

const MockShopFrameworkButtons = () => {
  const navigate = useNavigate();
  const signedIn = false;
  return (
    <Grid item xs={9} display="flex" justifyContent="right" alignItems="center">
      {["about us", "mock merch", "mock beers"].map((name) => (
        <Button
          theme={"dark"}
          text={name}
          callback={() => {
            navigate(`/${name}`);
          }}
        />
      ))}
      <Button
        theme={"light"}
        text={signedIn ? "my account" : "already have an account? sign in!"}
        callback={() => {
          navigate(signedIn ? "/my%20account" : "/log%20in");
        }}
      />
    </Grid>
  );
};

const MockShopFrameworkCart = () => {
  const noOfItems = 15;
  return (
    <Grid
      item
      xs={1}
      display="flex"
      justifyContent={"center"}
      alignItems={"center"}
    >
      <ItemCart n={noOfItems} />
    </Grid>
  );
};

export const MockShopFramework = () => {
  return (
    <AppBar
      position="fixed"
      className="mock-shop-app-bar"
      sx={{ backgroundColor: "#414141" }}
    >
      <Toolbar disableGutters>
        <Grid container columnSpacing={1}>
          <MockShopFrameworkLogo />

          <MockShopFrameworkButtons />

          <MockShopFrameworkCart />
        </Grid>
      </Toolbar>
    </AppBar>
  );
};
