import * as beers from "./mock-beers/mock-beers.js";
import * as merch from "./mock-merch/mock-merch.js";
import express from "express";
import cors from "cors";

const app = express();
const PORT = 8080;

app.use(
  cors({
    origin: ["https://www.section.io", "http://localhost:3000"],
    methods: ["GET", "POST", "DELETE", "UPDATE", "PUT", "PATCH"],
  })
);

app.listen(PORT, () => {
  console.log(`...listening on ${PORT}`);
});

app.get("/", (_, response) => {
  response.send({ secret: "Hello World!" });
});

app.get("/about%20us", (_, response) => {
  response.send({
    code: 0,
    payload: "excellent you should really use this product",
  });
});

app.get("/mock%20beers", (_, response) => {
  response.send(beers.getMockBeers());
});

app.get("/mock%20merch", (_, response) => {
  response.send(merch.getMockMerch());
});

app.put("/mailing%20list", (request, response) => {
  console.log("sending a put request fam");
  console.log(request);
  response.send({ code: 0, payload: {} });
});
